package router

import (
	"fmt"
	"time"

	"github.com/gofiber/fiber/v2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB = connectDatabase()

// #region Route CURD

func Create(c *fiber.Ctx) error {

	jsonStr := new(CustomerRequire)

	if err := c.BodyParser(jsonStr); err != nil {
		return err
	}

	create(db, jsonStr)

	return c.SendString("Create has done.")
}

func Update(c *fiber.Ctx) error {

	jsonStr := new(UpdateCustomerRequire)

	if err := c.BodyParser(jsonStr); err != nil {
		return err
	}
	update(db, jsonStr)

	return c.SendString("Update has done.")
}

func GetAll(c *fiber.Ctx) error {
	var ret = query(db)
	return c.JSON(ret)
}

func GetDataById(c *fiber.Ctx) error {

	fmt.Println(c.Params("id"))
	var ret = queryById(db, c)
	return c.JSON(ret)
}

func Delete(c *fiber.Ctx) error {
	jsonStr := new(UpdateCustomerRequire)

	fmt.Println("1")

	if err := c.BodyParser(jsonStr); err != nil {
		return err
	}
	fmt.Println("2")
	delete(db, jsonStr)
	fmt.Println("3")
	return c.SendString("Delete has done.")
}

// #endregion

// #region Private Function
func create(_db *gorm.DB, jsonStr *CustomerRequire) {
	var repo CustomerSupportRequests

	repo.RequestId = "6867f511-dceb-4850-9721-1107c33bc3ac"
	repo.FirstName = jsonStr.FirstName
	repo.LastName = jsonStr.LastName
	repo.Email = jsonStr.Email
	repo.IssueTitle = jsonStr.IssueTitle
	repo.DetailText = jsonStr.DetailText
	repo.CreatedDateTime = time.Now()

	fmt.Println(repo)
	_db.Create(repo)
}

func update(_db *gorm.DB, jsonStr *UpdateCustomerRequire) {
	data := CustomerSupportRequests{RequestId: jsonStr.Uuid}
	_db.Find(&data)
	fmt.Println(data)

	//binding the record wiht new value.
	data.FirstName = "mathus1"
	data.LastName = "nakpansua1"
	data.Email = "mathus057@gmail.com"
	_db.Save(&data)

}

func delete(_db *gorm.DB, jsonStr *UpdateCustomerRequire) {

	//binding rec wiht id
	data := CustomerSupportRequests{RequestId: jsonStr.Uuid}

	//binding data from record
	_db.Find(&data)

	fmt.Println(data)

	_db.Unscoped().Delete(&data)
}

func query(_db *gorm.DB) []CustomerSupportRequests {
	var customerSupportRequestsRepo []CustomerSupportRequests
	_db.Find(&customerSupportRequestsRepo)
	fmt.Println(customerSupportRequestsRepo)
	return customerSupportRequestsRepo
}

func queryById(_db *gorm.DB, c *fiber.Ctx) CustomerSupportRequests {
	var ret CustomerSupportRequests

	_db.First(&ret, "request_id = ?", c.Params("id"))
	fmt.Println(ret)
	return ret
}

func connectDatabase() *gorm.DB {
	dsn := "host=localhost user=postgres password=password dbname=custormer_support search_path=customer_support_request port=5432 sslmode=disable TimeZone=Asia/Bangkok"
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	database.AutoMigrate(&CustomerSupportRequests{})
	if err != nil {
		fmt.Println(err)
	}
	return database
}

// #endregion

// #region Declare Struct
type CustomerSupportRequests struct {
	RequestId       string    `gorm:"primaryKey"`
	FirstName       string    `gorm:"first_name"`
	LastName        string    `gorm:"last_name"`
	Email           string    `gorm:"email"`
	IssueTitle      string    `gorm:"issue_title"`
	DetailText      string    `gorm:"created_datetime"`
	CreatedDateTime time.Time `gorm:"column:created_datetime"`
}

type CustomerRequire struct {
	FirstName  string `json:"firstName" validate:"required"`
	LastName   string `json:"lastName" validate:"required"`
	Email      string `json:"email" validate:"required"`
	IssueTitle string `json:"issueTitle" validate:"required"`
	DetailText string `json:"detailText" validate:"required"`
}

type UpdateCustomerRequire struct {
	Uuid       string `json:"uuid" validate:"required"`
	FirstName  string `json:"firstName" validate:"required"`
	LastName   string `json:"lastName" validate:"required"`
	Email      string `json:"email" validate:"required"`
	IssueTitle string `json:"issueTitle" validate:"required"`
	DetailText string `json:"detailText" validate:"required"`
}

//#endregion
