package main

import (
	"demo2/router"

	"github.com/gofiber/fiber/v2"
)

func main() {

	app := fiber.New()
	routing(app)
	app.Listen(":4000")

}

func routing(app *fiber.App) {
	app.Post("/create", router.Create)
	app.Get("/", router.GetAll)
	app.Get("/:id", router.GetDataById)
	app.Put("/update", router.Update)
	app.Delete("/delete", router.Delete)
}
